package edu.CECAR.logica;

import java.util.ArrayList;
import java.util.Arrays;

public class Primos {

	private static ArrayList<Integer> primos;
	private static int residuosCero;

	private static void getPrimos(int l) {

		primos = new ArrayList<Integer>();

		for (int dividendo = 1; dividendo <= l; dividendo++) {
			residuosCero = 0;
			for (int divisor = 1; divisor <= dividendo; divisor++) {

				if ((dividendo / divisor) < divisor) {
					primos.add(dividendo);
					divisor = dividendo; // Salga del ciclo del divisor, dividendo es primo.
				} else if ((dividendo % divisor == 0)) {
					residuosCero++;
					if (residuosCero == 2) {
						if (divisor < dividendo) {
							divisor = dividendo; // Salga del ciclo del divisor, dividendo no es primo.
							// System.out.println(d + " no es primo.");
						} else {
							primos.add(dividendo);
							divisor = dividendo; // Salga del ciclo del divisor, dividendo es primo.
						}

					}
				}
			}

		}
	}

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		getPrimos(10000);
		System.out.println("Tiempo ejecucion: " + (System.currentTimeMillis() - inicio) + " ms");
		System.out.println("Primos: " + Arrays.asList(primos));
	}

}
